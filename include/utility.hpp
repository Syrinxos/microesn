#pragma once

#include <Eigen/Sparse>
#include <Eigen/Dense>

namespace microESN {

typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::MatrixXd DeMat;
typedef Eigen::VectorXd DeVec;


enum InitParameters{
	inputSize,
	outputSize,
	reservoirSize,
	spectralRadius,
	connectivity,
	inputShift,
	inputScaling,
	inputConnectivity,
	fbConnectivity,
	fbScaling,
	fbShift,
	leakingRate,
	tikhonovFactor,
	noise,
	reservoirAct,
	outputAct,
	trainingAlg,
	simulationAlg
};

class Rand {
	public:
  		static void initSeed() { srand(time(0)); }

  		static double uniform(float min=-1, float max=1) {
			double tmp = std::rand() / (double(RAND_MAX)+1); // between [0|1)
			return tmp*(max-min) + min;
  		}

		static void uniform(DeVec &vec, float min = -1, float max = 1){
			for(int i = 0; i < vec.rows(); ++i){
				vec.data()[i] = std::rand() / (double(RAND_MAX) + 1);
			}
			vec *= (max - min);
			vec += DeVec::Constant(vec.rows(), min);
		}
};
}