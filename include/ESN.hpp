#pragma once
#include <map>
#include <vector>
#include <iostream>
#include <random>
#include <functional>
#include <Spectra/SymEigsSolver.h>

#include "utility.hpp"
#include "activations.hpp"
#include "train.hpp"
#include "simulate.hpp"
#include "microException.hpp"

namespace microESN {
class ESN {
	public: 
	ESN(ESN& esn);
	ESN(DeMat wInput, DeMat wOutput, SpMat wReservoir, DeMat wFeedback, DeVec states, std::map<InitParameters, double> map);
	ESN(std::map<InitParameters, double>);
	~ESN();
	void printInfo();
	
	inline int getNInput() {return nInput;}
	inline int getNOutput() {return nOutput;}
	inline int getNReservoir() {return nReservoir;}
	inline DeMat& getWOutput () {return wOutput;}
	inline DeMat& getWInput() { return wInput; }
	inline DeMat& getWBack() { return wBack; }
	inline SpMat& getWReservoir() { return wReservoir; }
	inline DeVec& getState() { return states; }
	inline float getNoise() { return noise; }

	inline void setState(DeVec newState) { states = newState; }

	inline void reservoirActivation(double *data, int size) { reservoirAct(data, size); }
	inline void outputActivation(double *data, int size) { outputAct(data, size); }

	void trainESN(const DeMat &in, const DeMat &out, int washout);
	void simulateESN(const DeMat &in, DeMat &out);
	void adaptESN(const DeMat &in, const DeMat &out);

	private:
	ESN();
	void parametersSetup();
	void initWeights();
	void setReservoirAct(ActivationFunction actFun);
	void setOutputAct(ActivationFunction actFun);
	void setSimulationAlgorithm(SimAlgorithm alg);
	void setTrainingAlgorithm(TrainAlgorithm alg);
	void checkParameters();
	
	std::map<InitParameters, double> initParameters;

	DeMat wInput;
	SpMat wReservoir;
	DeMat wOutput;
	DeMat wBack;
	DeVec states;

	int nInput;
	int nOutput;
	int nReservoir;
	float spectralRadius;
	float connectivity;
	float inputShift;
	float inputScaling;
	float inputConnectivity;
	float fbConnectivity;
	float fbScaling;
	float fbShift;
	float leakingRate;
	float tikhonovFactor;
	float noise;
	float deltaFactor = 0.5;
	float rememberFactor = 0.6f;

	SimBase *sim = nullptr;
	TrainBase *training = nullptr;

	std::function<void(double*, int)> reservoirAct;
	std::function<void(double*, int)> outputAct;
}; 
}

