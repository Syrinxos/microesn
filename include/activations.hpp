#pragma once
#include "utility.hpp"
#include <cmath>

namespace microESN {
enum ActivationFunction{
	LINEAR,
	TANH,
	SIGMOID
};


inline void act_linear(double *data, int size) {}

inline void act_tanh(double *data, int size) {
	for(int i = 0; i < size; i++){
		data[i] = tanh(data[i]);
	}
}

inline void act_sigmoid(double *data, int size) {
	for(int i = 0; i < size; i++){
		double tmp = 1.0 / (1.0 + exp(data[i]));
		data[i] = tmp;
	}
}
}