#pragma once
#include "utility.hpp"
#include "microException.hpp"
#include "simulate.hpp"

#include <iostream>
#include <memory>
#include <Eigen/LU>

namespace microESN {
enum TrainAlgorithm {
	TRAIN_PI, // Offline, pseudo inverse
	TRAIN_RIDGEREG, // Ridge regression
};

class ESN;
class TrainBase {
	public:
	TrainBase();
	TrainBase(int nInput_, int nOutput_, int nReservoir_, DeMat &wOutput_, DeVec &state, SimBase *sim_) : nInput{nInput_}, nOutput{nOutput_}, 
				nReservoir{nReservoir_}, wOutput{wOutput_}, sim{sim_}, state{state} {};
	~TrainBase() {};

	void collectStates(const DeMat &in, const DeMat &out, int washout, float noise);
	inline void setOutputAct(std::function<void(double*, int)> outputAct) { this->outputAct = outputAct; }

	virtual void train(const DeMat &in, const DeMat &out, int washout, float noise) = 0;

	protected:
	void checkParams(const DeMat &in, const DeMat &out, int washout);
	inline void clearData(){
		M.resize(1, 1); O.resize(1, 1);
	}
	// Network States and inputs over all timesteps
	DeMat M;
	// Outputs over all timesteps
	DeMat O;

	int nInput;
	int nOutput;
	int nReservoir;
	DeVec &state;
	DeMat &wOutput;
	std::function<void(double*, int)> outputAct;

	SimBase *sim = nullptr;
};

/* 
* Offline training algorithm using pseudo inverse.
* More expensive than TrainLeastSquare.
* 1. Collects the internal states and desired outputs;
* 2. Computes output weights using the pseudo inverse;
*/
class TrainPI : public TrainBase {
	public: 
	TrainPI(int nInput_, int nOutput_, int nReservoir_, DeMat &wOutput, DeVec &state, SimBase *sim_) : TrainBase(nInput_, nOutput_, nReservoir_, wOutput, state, sim_) {} ;
	~TrainPI() {} 

	virtual void train(const DeMat &in, const DeMat &out, int washout, float noise);
};

/*
* Offline algorithm with Ridge Regression/ Tikhonov Reg.
* Train an ESN in the same way as TrainLeastSquare or TrainPI, but it uses a
* regularization factor to calculate the output weights, where it tires to get them as small as possible.
* tikhonovFactor is the init parameter that regulates the regularization.
*/	
class TrainRidgeReg : public TrainBase {
	public:
	TrainRidgeReg(int nInput_, int nOutput_, int nReservoir_, DeMat &wOutput, DeVec &state, SimBase *sim_, float tikhonov) : TrainBase(nInput_, nOutput_, nReservoir_, wOutput, state, sim_) {
		tikhonovFactor = tikhonov;
	}
	virtual ~TrainRidgeReg() {} 

	virtual void train(const DeMat &in, const DeMat &out, int washout, float noise);

	private:
	float tikhonovFactor;
};
}