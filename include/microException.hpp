#pragma once

#include <string>

namespace microESN {
class microException {
	protected:
	std::string message;

	public:
	microException(const std::string &msg) : message(msg) {}
	virtual ~microException() {}

	virtual std::string what() {return message;}	
};
}