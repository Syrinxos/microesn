#pragma once
#include <memory>

#include "utility.hpp"
#include "microException.hpp"

namespace microESN {
enum SimAlgorithm {
  SIM_STD,       //standard simulation \sa class SimStd
  SIM_LI,        //simulation with leaky integrator neurons class SimLI
};

class ESN;

class SimBase{
	public:
	SimBase(int nInput, int nOutput, int nReservoir, DeMat &wInput, DeMat &wOutput, DeMat &wBack, SpMat &wReservoir);
	 ~SimBase() {};

	virtual void simulate(const DeMat &in, DeMat &out, DeVec &state, float noise = 0.f) = 0;
	void reallocate();

	void setReservoirActivation(std::function<void(double*, int)> activation);
	void setOutputActivation(std::function<void(double*, int)> activation);

	DeMat lastOut;
	DeVec tmp;
	std::function<void(double*, int)> reservoirAct;
	std::function<void(double*, int)> outputAct;
	DeMat &wInput, &wOutput, &wBack;
	SpMat &wReservoir;

	protected:
	int nInput, nOutput, nReservoir;
};

class SimStd : public SimBase {
	public:
	SimStd(int nInput, int nOutput, int nReservoir, DeMat &wInput, DeMat &wOutput, DeMat &wBack, SpMat &wReservoir) : SimBase(nInput, nOutput, nReservoir, wInput, wOutput, wBack, wReservoir) {}

	virtual void simulate(const DeMat &in, DeMat &out, DeVec &state, float noise = 0.f);
};

/*
* By using Leaky Integrator neurons;
* Useful for learning slow dynamical systems.
* The spectral radius should not be bigger than the leaking rate.
*/
class SimLI : public SimBase{
	public:
	SimLI(int nInput, int nOutput, int nReservoir, DeMat &wInput, DeMat &wOutput, DeMat &wBack, SpMat &wReservoir, float leakingRate) : SimBase(nInput, nOutput, nReservoir, wInput, wOutput, wBack, wReservoir) {
		this->leakingRate = leakingRate;
	}
	virtual ~SimLI() {}
	virtual void simulate(const DeMat &in, DeMat &out, DeVec &state, float noise = 0.f);

	private: 
	float leakingRate;
};
}