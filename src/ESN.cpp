#include "ESN.hpp"

namespace microESN {

ESN::ESN(){
	std::cout << "Setup..." << std::endl;
	Rand::initSeed();
	// Some default values;
	sim = 0;
	training = 0;
	noise = 0;
	nReservoir = 10;
	nInput = 1;
	nOutput = 1;
	connectivity = 0.8f;
	spectralRadius = 0.8f;
	inputConnectivity = 0.8;
	inputScaling = 1.f;
	inputShift = 0.f;
	fbConnectivity = 0.f;
	fbScaling = 1.f;
	fbShift = 0.f;
	setReservoirAct(ActivationFunction::TANH);
	setOutputAct(ActivationFunction::LINEAR);
	setSimulationAlgorithm(SimAlgorithm::SIM_STD);
	setTrainingAlgorithm(TrainAlgorithm::TRAIN_PI);
}

ESN::ESN(std::map<InitParameters, double> map) : initParameters{map}{
	ESN();
	parametersSetup();
	initWeights();
}

ESN::ESN(DeMat wInput, DeMat wOutput, SpMat wReservoir, DeMat wFeedback, DeVec states, std::map<InitParameters, double> map) : initParameters{map}, wInput{wInput}, wOutput{wOutput}, wReservoir{wReservoir}, wBack{wFeedback}, states{states} {
	ESN();
	parametersSetup();
}

ESN::ESN(ESN &src){
	ESN(src.initParameters);
}

ESN::~ESN(){
	if(training) delete training;
	if(sim) delete sim;
}

void ESN::initWeights(){

	wOutput = DeMat::Zero(nOutput, nInput + nReservoir);
	states = DeVec::Zero(nReservoir);

	// Input Weights
	wInput = DeMat::Zero(nReservoir, nInput);
	float nrn = nReservoir * inputConnectivity * nInput - 0.5;
	for(int i = 0; i < nrn; i++){
		wInput.data()[i] = Rand::uniform();
	}
	std::random_shuffle(wInput.data(), wInput.data() + wInput.rows() * wInput.cols());
	wInput *= inputScaling;
	wInput += DeMat::Constant(wInput.rows(), wInput.cols(), inputShift);
	//std::cout << "wInput:\n" << wInput << std::endl;

	// Feedback Weights
	wBack = DeMat::Zero(nReservoir, nOutput);
	nrn = nReservoir * fbConnectivity * nOutput - 0.5f;
	for (int i = 0; i < nrn; i++){
		wBack.data()[i] = Rand::uniform();
	}
	std::random_shuffle(wBack.data(), wBack.data() + wBack.rows() * wBack.cols());
	wBack *= fbScaling;
	wBack += DeMat::Constant(wBack.rows(), wBack.cols(), fbShift);
	//std::cout << "wBack:\n" << wBack << std::endl;

	// Reservoir Weights
	DeMat Wtmp = DeMat::Zero(nReservoir, nReservoir);
	nrn = nReservoir * nReservoir * connectivity - 0.5;
	for(int i = 0; i < nrn; i++){
		Wtmp.data()[i] = Rand::uniform();
	}
	std::random_shuffle(Wtmp.data(), Wtmp.data() + (Wtmp.rows() * Wtmp.cols()));
	Spectra::DenseSymMatProd<double> op(Wtmp);
	Spectra::SymEigsSolver<double, Spectra::LARGEST_ALGE, Spectra::DenseSymMatProd<double> > eigs(&op, 1, nReservoir);
	eigs.init();
	int nconv = eigs.compute();
	DeVec eval;
	if(eigs.info() == Spectra::SUCCESSFUL){
		eval = eigs.eigenvalues();
	}else{
		throw microException("ESN::initWeights: Couldn't compute eigenvalues of the reservoir weights.");
	}
	double maxEig = eval(0);
	if(maxEig == 0){
		throw microException("ESN::initWeights: Maximum eigenvalue is zero, try again.");
	}
	Wtmp *= (spectralRadius / maxEig);
	wReservoir = Wtmp.sparseView();
	//std::cout << "wReservoir\n" << Wtmp << std::endl;
}

void ESN::printInfo() {
	std::cout << "-------------- Current status -------------" << std::endl;
	std::cout << "wInput" << std::endl;
	std::cout << wInput << std::endl << std::endl;

	std::cout << "wOutput" << std::endl;
	std::cout << wOutput << std::endl << std::endl;

	std::cout << "wBack" << std::endl;
	std::cout << wBack << std::endl << std::endl;
	
	std::cout << "wReservoir" << std::endl;
	//std::cout << wReservoir.toDense() << std::endl << std::endl;
	std::cout << "-------------- ------- ------ -------------" << std::endl;
}

void ESN::parametersSetup(){
	checkParameters();
	nInput = initParameters[InitParameters::inputSize];
	nReservoir = initParameters[InitParameters::reservoirSize];
	nOutput = initParameters[InitParameters::outputSize];
	spectralRadius = initParameters[InitParameters::spectralRadius];
	
	connectivity = initParameters[InitParameters::connectivity];
	
	inputShift = initParameters[InitParameters::inputShift];
	inputScaling = initParameters[InitParameters::inputScaling]; 
	inputConnectivity = initParameters[InitParameters::inputConnectivity];
	
	fbConnectivity = initParameters[InitParameters::fbConnectivity];
	fbScaling = initParameters[InitParameters::fbScaling];
	fbShift = initParameters[InitParameters::fbShift];

	noise = initParameters[InitParameters::noise];
	setReservoirAct(static_cast<ActivationFunction> (initParameters[InitParameters::reservoirAct]));
	setOutputAct(static_cast<ActivationFunction> (initParameters[InitParameters::outputAct]));
	setSimulationAlgorithm(static_cast<SimAlgorithm> (initParameters[InitParameters::simulationAlg]));
	setTrainingAlgorithm(static_cast<TrainAlgorithm> (initParameters[InitParameters::trainingAlg]));
}	

void ESN::adaptESN(const DeMat &in, const DeMat &out){
	int steps = in.cols();

	DeMat P = this->deltaFactor * DeMat::Identity(steps, nReservoir + nInput);
	DeVec eps, c, k;
	DeMat simOut = DeMat::Zero(in.rows(), 1);
	DeVec joined = DeVec(nInput + states.rows());
	for(int i = 0; i < steps; i++){
		sim->simulate(in.col(i), simOut, states, noise);
		joined << states, in.col(1);
		eps = out.col(i) - wOutput * joined;
		c = P * joined;
		DeVec tmp = joined.transpose() * c.block(0, 0, joined.rows(), joined.cols());
		k = c / (tmp(0,0));
		//P (t) = λ RLS P (t−1) − k(t)i (t) T P (t−1)
		tmp = k.block(0, 0, joined.rows(), 1);// * joined.transpose();
		DeMat tmp2 = tmp * joined.transpose();
		P = (P - tmp2(0,0) * P);
		wOutput = wOutput + eps(0,0) * k.block(0, 0, joined.rows(), 1).transpose();
		/*std::cout << "eps: " << std::endl << eps << std::endl;
		std::cout << "Joined: " << std::endl << joined << std::endl;
		std::cout << "c: " << std::endl << c.block(0,0,nReservoir + nInput, 1) << std::endl;
		std::cout << "k: " << std::endl << k.block(0,0,nReservoir + nInput, 1) << std::endl;		
		std::cout << "P: " << std::endl << P(3,3) << std::endl;
		std::cout << "simOut: " << simOut << std::endl;

		std::cout << "----------------" << std::endl;
		std::cin.get();
		*/
	}
}

void ESN::checkParameters(){
	float tmp;
	tmp = initParameters[InitParameters::connectivity];
	if(tmp <= 0 || tmp > 1)
		throw microException("ESN::parametersSetup: Connectivity must be within (0|1].");
	
	tmp = initParameters[InitParameters::spectralRadius];
	if(tmp < 0)
		throw microException("ESN::parametersSetup: Spectral Radius must be >= 0.");

	tmp = initParameters[InitParameters::inputConnectivity];
	if(tmp < 0 || tmp > 1)
		throw microException("ESN::parametersSetup: Input connectivity must be within [0, 1].");
	
	tmp =  initParameters[InitParameters::fbConnectivity];
	if(tmp < 0 || tmp > 1)
		throw microException("ESN:parametersSetup: Feedback connectivity must be within [0, 1]");
	
	if(initParameters[InitParameters::simulationAlg] == SIM_LI){
		if(initParameters.find(InitParameters::leakingRate) == initParameters.end())
			throw microException("ESN::checkParameteres: No Leaking Rate given, but using Leaking Integrator Simulation.");
		tmp = initParameters[InitParameters::leakingRate];
		if(tmp < 0)
			throw microException("ESN::checkParameters: Leaking Rate must be >= 0!");
	}
	if(initParameters[InitParameters::trainingAlg] == TRAIN_RIDGEREG){
		if(initParameters.find(InitParameters::tikhonovFactor) == initParameters.end())
			throw microException("ESN::checkParameters: No tikhonov factor given, but using Ridge Regression Training.");
		tmp = initParameters[InitParameters::tikhonovFactor];
		if(tmp < 0)	
			throw microException("ESN::checkParameters: Tikhonov Factor must be >=0!");
	}

}

void ESN::setReservoirAct(ActivationFunction i){
	switch (i) {
		case ActivationFunction::LINEAR:
			reservoirAct = &act_linear;
		break;
		case ActivationFunction::SIGMOID:
			reservoirAct = &act_sigmoid;
		break;
		case ActivationFunction::TANH:
			reservoirAct = &act_tanh;
		break;
	}
}


void ESN::setOutputAct(ActivationFunction i){
	switch (i) {
		case ActivationFunction::LINEAR:
			outputAct = &act_linear;
		break;
		case ActivationFunction::SIGMOID:
			outputAct = &act_sigmoid;
		break;
		case ActivationFunction::TANH:
			outputAct = &act_tanh;
		break;
	}
}

void ESN::trainESN(const DeMat &in, const DeMat &out, int washout) {
	training->train(in, out, washout, noise);
}

void ESN::simulateESN(const DeMat &in, DeMat &out){
	sim->simulate(in, out, states, noise);
}

void ESN::setSimulationAlgorithm(SimAlgorithm alg){
	switch(alg){
		case SimAlgorithm::SIM_STD:
			if(sim) delete sim;
			sim = new SimStd(nInput, nOutput, nReservoir, wInput, wOutput, wBack, wReservoir);
		break;
		case SimAlgorithm::SIM_LI:
			if(sim) delete sim;
			sim = new SimLI(nInput, nOutput, nReservoir, wInput, wOutput, wBack, wReservoir, initParameters[InitParameters::leakingRate]);
		break;
	}
	if(sim) {
		sim->setReservoirActivation(reservoirAct);
		sim->setOutputActivation(outputAct);
	}
}

void ESN::setTrainingAlgorithm(TrainAlgorithm alg){
	switch(alg){
		case TrainAlgorithm::TRAIN_PI:
		if(training) delete training;
		training = new TrainPI(nInput, nOutput, nReservoir, wOutput, states, sim);
		break;
		case TrainAlgorithm::TRAIN_RIDGEREG:
		if(training) delete training;
		training = new TrainRidgeReg(nInput, nOutput, nReservoir, wOutput, states, sim, initParameters[InitParameters::tikhonovFactor]);
		break;
	}
	if(training) {
		training->setOutputAct(outputAct);
	}
}

}