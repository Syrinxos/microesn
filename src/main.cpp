#include "ESN.hpp"
#include "utility.hpp"
#include <map>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <iostream>

using namespace microESN;

void Test(std::map<InitParameters, double>);
void normalize(DeMat &in);

int main(){
	std::map<InitParameters, double> map;
	map[InitParameters::inputSize] = 1;
	map[InitParameters::outputSize] = 1;
	map[InitParameters::reservoirSize] = 500;
	map[InitParameters::inputScaling] = 0.35;
	map[InitParameters::inputShift] = 0.0;
	map[InitParameters::noise] = 0.0;
	map[InitParameters::reservoirAct] = ActivationFunction::TANH;
	map[InitParameters::outputAct] = ActivationFunction::LINEAR;
	map[InitParameters::simulationAlg] = SimAlgorithm::SIM_STD;
	map[InitParameters::trainingAlg] = TrainAlgorithm::TRAIN_PI;
	map[InitParameters::spectralRadius] = 0.75;
	map[InitParameters::leakingRate] = 1;
	map[InitParameters::tikhonovFactor] = exp(-8);
	map[InitParameters::connectivity] = 0.5f;
	map[InitParameters::inputConnectivity] = 1;
	map[InitParameters::fbConnectivity] = 0;
	map[InitParameters::fbScaling] = 0;
	map[InitParameters::fbShift] = 0;
	try{
		Test(map);
	}
	catch(microException e){
		std::cout << e.what() << std::endl;
	}
}

void Test(std::map<InitParameters, double> map){
	int size = 10092;
	int trainSize = 5000;
	DeMat datasIn(1, size);
	DeMat datasOut(1, size);
	std::ifstream inFile;
	inFile.open("datasets/Laser/LaserPredictionOneStepAhead.csv");

	std::string line;
	int j = 0;
	while (inFile >> line) {
		int i = 0;
		std::stringstream ss(line);
		std::string token;
		while(std::getline(ss, token, ',')) {
				if(i == 1) datasOut(0, j) = std::stod(token);
				else datasIn(0, j) = std::stod(token);
			i++;
		}
		j++;
	}
	inFile.close();
	ESN e(map);
	// normalize(datasIn);
	// normalize(datasOut);
	datasIn /= 255;
	datasOut /= 255;
	DeMat in = datasIn.block(0, 0, 1, trainSize);
	DeMat testIn = datasIn.block(0, trainSize, 1, size-trainSize);
	DeMat out = datasOut.block(0, 0, 1, trainSize);
	DeMat testOut = datasOut.block(0, trainSize, 1, size-trainSize);	

	e.trainESN(in, out,  1000);
	DeMat testOutTEST = DeMat::Zero(1, size-trainSize);	
	e.simulateESN(testIn, testOutTEST);
	std::ofstream myfile;
	myfile.open ("resultLASER.csv");
	double sum = 0;
	for(int j = 0; j < testOutTEST.cols(); j++){
		std::cout << "Obtained: " << testOutTEST(0, j) << ", Expected: " << testOut(0,j) << std::endl;
		myfile << testOutTEST(0, j) << "," << testOut(0,j) << std::endl;
		sum += pow(testOutTEST(0, j) - testOut(0, j), 2);
	}
	myfile.close();
	std::cout << std::endl << "ERROR: " << (sum / (size-trainSize)) << std::endl;
}

void normalize(DeMat &in){
	// double meanIn = in.row(0).mean();
	// double stdDev = 0;
	// for(int i = 0; i < in.cols(); i++){
	// 	stdDev += pow(in(0, i) - meanIn, 2);
	// }
	// stdDev = stdDev / in.cols();
	// stdDev = sqrt(stdDev);
	// in.normalize();

	// for(int i = 0; i < in.cols(); i++){
	// 	in(0,i) = (in(0, i) - meanIn) / (stdDev);
	// }

	//(v-min)/(max-min) * (newmax-newmin) + newmin
	double max, min;

	for(int i = 0; i < in.cols(); i++){
		for(int j = 0; j < in.rows(); j++){
				max = in.col(i).maxCoeff();
				min = in.col(i).minCoeff();
				//std::cout << "max " << max << ", Min " << min << std::endl;
				in(j, i) = ((in(j, i) - min) / (max - min)) * 2 - 1;
		}
	}
}