#include "train.hpp"

namespace microESN {
void TrainBase::checkParams(const DeMat &in, const DeMat &out, int washout) {
	if (in.cols() != out.cols()){
		throw microException("TrainBase::checkParam: input and output must have the same column number!");
	}
	if (in.rows() != nInput){
		throw microException("TrainBase::checkParam: Wrong input row size!");
	}
	if (out.rows() != nOutput){
		throw microException("TrainBase::checkParam: Wrong output row size!");
	}
	if((in.cols() - washout) < nReservoir + nInput){
		throw microException("TrainBase::checkParam: too few training data!");
	}
}

void TrainBase::collectStates(const DeMat &in, const DeMat &out, int washout, float noise = 0.f){
	int steps = in.cols();
	O = DeMat::Zero(steps - washout, nOutput);
	M = DeMat::Zero(steps - washout, (nReservoir + nInput));
	DeMat simIn = DeMat::Zero(nInput, 1),
		simOut = DeMat::Zero(nOutput, 1);

	for(int n = 0; n < steps; ++n) {
		simIn.col(0) = in.col(n);
		sim->simulate(simIn, simOut, state, noise);
		sim->lastOut.col(0) = out.col(n);
		if(n >= washout){
			DeMat joined = DeMat(state.rows() + simIn.rows(), 1);
			joined << state, simIn.col(0);
			M.row(n - washout) = joined.transpose();

		}
	}
	DeMat tmp = (out.block(0, washout, out.rows(), steps-washout));
	O = tmp.transpose();
}

void TrainPI::train(const DeMat &in, const DeMat &out, int washout, float noise = 0.f){
	this->checkParams(in, out, washout);
	this->collectStates(in, out, washout, noise);

	DeMat tmpMat = M.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(O);
	this->wOutput = tmpMat.block(0, 0, M.cols(), tmpMat.cols());
	this->wOutput.transposeInPlace();
	this->clearData();
}

void TrainRidgeReg::train(const DeMat &in, const DeMat &out, int washout, float noise = 0.f){
	this->checkParams(in, out, washout);
	this->collectStates(in, out, washout, noise);

	DeMat t1 = DeMat::Zero(nReservoir + nInput, nReservoir + nInput);
	t1 = M.transpose() * M;
	t1 += DeMat::Identity(t1.rows(), t1.cols()) * tikhonovFactor;
	wOutput = t1.inverse() * M.transpose();
	t1 = wOutput * O;
	wOutput = t1.transpose();
	this->clearData();
}

}
