#include "simulate.hpp"
#include <iostream>
namespace microESN {
SimBase::SimBase(int nInput, int nOutput, int nReservoir, DeMat &wInput, DeMat &wOutput, DeMat &wBack, SpMat &wReservoir) 
	: nInput{nInput}, nOutput{nOutput}, nReservoir{nReservoir}, wInput{wInput}, wOutput{wOutput}, wBack{wBack}, wReservoir{wReservoir} {
	reallocate();
}

void SimBase::reallocate() {
	lastOut = DeMat::Zero(nOutput, 1);
	tmp.resize(nReservoir);
}

void SimStd::simulate(const DeMat &in, DeMat &out, DeVec &state, float noise){
	if(in.rows() != nInput){
		throw microException("SimStd::simulate: Input row size is wrong!");
	}
	if(out.rows() != nOutput){
		throw microException("SimStd::simulate: Output row size is wrong!");
	}
	if(in.cols() != out.cols()){
		throw microException("SimStd::simulate: Input col size is wrong!");
	}
	if(lastOut.rows() != nOutput){
		throw microException("SimStd::simulate: lastOut row size is wrong!");
	}

	int steps = in.cols();

	DeMat wOut1 = wOutput.block(0,0, wOutput.rows(), nReservoir);
	DeMat wOut2 = wOutput.block(0,nReservoir, wOutput.rows(), nInput);

	state = (wInput * in.col(0) + wReservoir * state + wBack * lastOut.col(0));

	reservoirAct(state.data(), state.rows());
	// Add noise
	Rand::uniform(tmp, -1.f * noise, noise);
	state += tmp;

	lastOut.col(0) = wOut1 * state + wOut2 * in.col(0);

	outputAct(lastOut.data(), lastOut.rows() * lastOut.cols());

	out.col(0) = lastOut.col(0);

	for(int n=1; n < steps; ++n){ 
		state = wInput * in.col(n) + wReservoir * state + wBack * out.col(n-1);
		reservoirAct(state.data(), state.rows());
		// Add noise
		Rand::uniform(tmp, -1.f * noise, noise);
		state += tmp;

		// output = Wout * [x; in]
		lastOut.col(0) = wOut1 * state + wOut2 * in.col(n);

		// output activation
		outputAct(lastOut.data(), lastOut.rows() * lastOut.cols() );
		out.col(n) = lastOut.col(0);
	}
}

void SimLI::simulate(const DeMat &in, DeMat &out, DeVec &state, float noise){
	if(in.rows() != nInput){
		throw microException("SimStd::simulate: Input row size is wrong!");
	}
	if(out.rows() != nOutput){
		throw microException("SimStd::simulate: Output row size is wrong!");
	}
	if(in.cols() != out.cols()){
		throw microException("SimStd::simulate: Input col size is wrong!");
	}
	if(lastOut.rows() != nOutput){
		throw microException("SimStd::simulate: lastOut row size is wrong!");
	}
	int steps = in.cols();

	DeMat wOut1 = wOutput.block(0,0, wOutput.rows(), nReservoir);
	DeMat wOut2 = wOutput.block(0,nReservoir, wOutput.rows(), nInput);

	tmp = state;
	state = (wInput * in.col(0) + wReservoir * tmp + wBack * lastOut.col(0));
	reservoirAct(state.data(), state.rows());
	state = (1. - leakingRate) * tmp + leakingRate*state;
	Rand::uniform(tmp, -1.*noise, noise);
	state += tmp;
	lastOut.col(0) = wOut1 * state + wOut2 * in.col(0);

	outputAct(lastOut.data(), lastOut.rows() * lastOut.cols());
	out.col(0) = lastOut.col(0);

	for(int n = 1; n < steps; n++){
		tmp = state;
		state = (wInput*in.col(n) + wReservoir * tmp + wBack * out.col(n-1));
		reservoirAct(state.data(), state.rows());
		state = (1. - leakingRate) * tmp + leakingRate*state;

		Rand::uniform(tmp, -1.*noise, noise);
		state += tmp;
		lastOut.col(0) = wOut1 * state + wOut2 * in.col(n);

		outputAct(lastOut.data(), lastOut.rows() * lastOut.cols());
		out.col(n) = lastOut.col(0);
	}	
}

void SimBase::setReservoirActivation(std::function<void(double*, int)> fun){
	reservoirAct = fun;
}

void SimBase::setOutputActivation(std::function<void(double*, int)> fun){
	outputAct = fun;
}
}