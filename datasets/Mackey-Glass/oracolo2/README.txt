Questa cartella contiene:

- Win: matrice dei pesi input-to-reservoir
- W: matrice dei pesi ricorrenti del reservoir
- Wout: matrice dei pesi reservoir-to-readout

* a differenza dell'oracolo1 in questo caso va usato un leaky parameter = 0.1 (come indicato sotto).

- allStates: stati di una ESN descritta dalle precedenti matrici per tutti i time steps del dataset Mackey-Glass
- trainOutput: output della ESN addestrata sui time step di training (dopo il washout)
- testOutput: output della ESN addestrata sui time step di test

Nota che le matrici e le atre strutture dati corrispondono ad una ESN con la seguente iperparametrizzazione:
reservoir dimension = 10
reservoir sparsity = 50%
bias input = 1
spectral radius = 0.9
leaky parameter = 0.1 (LI-ESN)
input scaling = 0.1
no readout regularization, i.e. lambda = 0

transiente = 1000 steps

Cosimo
Nella cartella ho aggiunto i seguenti file:
- allStatesCosimo.csv: analogo a allStates.csv con gli stati calcolati dalla mia implementazione; come indicato
  in esso, ho aggiunto alla fine di ogni colonna di X, il valore 1 del bias di sull'output per non avere problemi
  di compatibilità nei prodotti matriciali per calcolare Wout. I valori sono molto vicini a quelli presenti in allStates.csv
- esnOracolo1TrainedRR.txt: file contenente tutte le informazioni di una ESN istanziata con i parametri indicati sopra
  e le matrici caricate dai corrispondenti file in questa cartella. Addestrata con Ridge Regression con lambda = 0
- esnOracolo1TrainedPI.txt: analogo a esnOracolo1TrainedRR.txt ad eccezione del calcolo di Wout, fatto con PseudoInversa
  di Moore-Penrose di X
- trainOutputCosimoRR.csv: output della rete per i 4000 time step di training successivi ai primi 1000 di washout (scartati)
- testOutputCosimoRR.csv: output della rete per i 5000 time step di test
- trainOutputCosimoPI.csv: analogo trainOutputCosimoRR ma addestrato con PseudoInversa
- testOutputCosimoPI.csv: analogo a testOutputCosimoRR

Inoltre ho modificato il già esistente file Wout.csv mettendo in corrispondenza dei valori dei pesi di Wout
da ottenere, i valori dei pesi di Wout ottenuti dai due training effettuati

